'''
Created on Aug 14, 2018

@author: minal_t
'''

from flask import request
from dateutil.parser import parse
from server import app
from bson import json_util
from dao import newsDetailsDao
from extractors import dataextractors

@app.route('/getAllNews')
def getAllNews():
    result = newsDetailsDao.getAllNews()
    return json_util.dumps(result)

@app.route('/getNewsDateRange')
def getNewsDateRange():
    result = dataextractors.getNewsDateRange()
    return json_util.dumps(result)

@app.route('/getPlaceList')
def getPlaceList():
    result = newsDetailsDao.getPlaceList()
    return json_util.dumps(result)