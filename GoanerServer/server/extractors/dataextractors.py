'''
Created on Aug 21, 2018

@author: minal_t
'''
from dao import newsDetailsDao
import json
import sys,codecs

if sys.stdout.encoding != 'utf-8':
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')
  
def getNewsDateRange():
#     datesList = newsDetailsDao.getDistinctNewsDates()
#     dateArray = []
#     resultArray = {}
#     for dates in datesList:
#         dateStr = dates.strftime('%Y-%m-%d')
#         if dateStr not in dateArray:
#             dateArray.append(dateStr)
#             
#     for dateStr in dateArray:
#         resultArray[dateStr] = newsDetailsDao.getNewsDateRange(dateStr)
#     return resultArray
    resultArray = {}
    newsDict = newsDetailsDao.getAllNews()
    for dateStr in newsDict:
        resultArray[dateStr["key"]] = dateStr["value"]
    return resultArray