'''
Created on Jul 26, 2018

@author: minal_t
'''
from pymongo import MongoClient

dbUrl = 'localhost'
dbport = 27017


def mongoConnection(databaseName):
    client = MongoClient(dbUrl, dbport)
    db = client[databaseName]
    return db