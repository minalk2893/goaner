'''
Created on Aug 14, 2018

@author: minal_t
'''
import dbhandler
from datetime import datetime,timedelta
import pymongo
from dateutil.parser import parse

dbName = "db_newspaper_scraper"
collectionName = "collection_newspaper_new"

def getAllNews():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    return collections.find({}).sort([("key",pymongo.DESCENDING)])
    #return collections.find().sort([("imageUrl",pymongo.DESCENDING)])

def getDistinctNewsDates():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    return collections.distinct("timestamp")

def getPlaceList():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    return collections.distinct("place")

def getNewsDateRange(dateValue):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    fromDate = datetime.strptime(parse(dateValue).strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
    toDate = datetime.strptime(((parse(dateValue) + timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")),"%Y-%m-%d %H:%M:%S")
    return collections.find({"timestamp":{"$gte":fromDate,"$lte":toDate}})