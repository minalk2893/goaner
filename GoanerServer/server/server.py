from flask import Flask  # From module flask import class Flask
from flask_cors import CORS

app = Flask(__name__)    # Construct an instance of Flask class for our webapp
app.debug = True
CORS(app)

from approutes.routes import *

if __name__ == '__main__':  # Script executed directly?
    app.run()  # Launch built-in web server and run this Flask webapp