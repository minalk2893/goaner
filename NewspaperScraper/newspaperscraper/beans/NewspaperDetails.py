class NewspaperDetails:
    def __init__(self,headline,timestamp,place,firstPara,articleUrl,category,imageUrl,source):
        self.headline = headline
        self.timestamp = timestamp
        self.place = place
        self.firstPara = firstPara
        self.articleUrl = articleUrl
        self.category = category
        self.imageUrl = imageUrl
        self.source = source
        
    def get_id(self):
        return self.__id


    def set_id(self, value):
        self.__id = value


    def del_id(self):
        del self.__id


    def get_source(self):
        return self.__source


    def set_source(self, value):
        self.__source = value


    def del_source(self):
        del self.__source


    def get_headline(self):
        return self.__headline


    def get_timestamp(self):
        return self.__timestamp


    def get_place(self):
        return self.__place


    def get_first_para(self):
        return self.__firstPara


    def get_article_url(self):
        return self.__articleURL


    def get_category(self):
        return self.__category


    def get_image_url(self):
        return self.__imageUrl


    def set_headline(self, value):
        self.__headline = value


    def set_timestamp(self, value):
        self.__timestamp = value


    def set_place(self, value):
        self.__place = value


    def set_first_para(self, value):
        self.__firstPara = value


    def set_article_url(self, value):
        self.__articleURL = value


    def set_category(self, value):
        self.__category = value


    def set_image_url(self, value):
        self.__imageUrl = value


    def del_headline(self):
        del self.__headline


    def del_timestamp(self):
        del self.__timestamp


    def del_place(self):
        del self.__place


    def del_first_para(self):
        del self.__firstPara


    def del_article_url(self):
        del self.__articleURL


    def del_category(self):
        del self.__category


    def del_image_url(self):
        del self.__imageUrl

    headline = property(get_headline, set_headline, del_headline, "headline's docstring")
    timestamp = property(get_timestamp, set_timestamp, del_timestamp, "timestamp's docstring")
    place = property(get_place, set_place, del_place, "place's docstring")
    firstPara = property(get_first_para, set_first_para, del_first_para, "firstPara's docstring")
    articleUrl = property(get_article_url, set_article_url, del_article_url, "articleUrl's docstring")
    category = property(get_category, set_category, del_category, "category's docstring")
    imageUrl = property(get_image_url, set_image_url, del_image_url, "imageUrl's docstring")
    source = property(get_source, set_source, del_source, "source's docstring")
    id = property(get_id, set_id, del_id, "id's docstring")
    
    def __str__(self):
        return "{ %s,\n %s,\n %s,\n %s,\n %s,\n %s,\n %s }" %(self.headline,self.timestamp,self.place,self.firstPara,self.articleUrl,self.category,self.imageUrl)
    
    def get_news_as_json(self):
        return {
            'headline':self.headline,
            'timestamp':self.timestamp,
            'place':self.place,
            'firstPara':self.firstPara,
            'articleUrl':self.articleUrl,
            'category':self.category,
            'imageUrl':self.imageUrl,
            'source':self.source
        }
        
    def get_news_as_json2(self):
        return {
            'headline':self.headline,
            'timestamp':self.timestamp,
            'place':self.place,
            'firstPara':self.firstPara,
            'articleUrl':self.articleUrl,
            'category':self.category,
            'imageUrl':self.imageUrl,
            'source':self.source,
            '_id':self.id
        }
        
    def get_news_as_string(self):
        return "{\"text\":\"%s:%s\"}" %(self.headline,self.firstPara)
