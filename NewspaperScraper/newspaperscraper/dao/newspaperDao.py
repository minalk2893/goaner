'''
Created on Jul 26, 2018

@author: minal_t
'''

import dbhandler
from datetime import datetime,timedelta
from dateutil.parser import parse
from bson import json_util
import pprint

dbName = "db_newspaper_scraper"
collectionName = "collection_newspaper"

def insertNews(newsList):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    fromDate = datetime.strptime(datetime.utcnow().strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
    toDate = datetime.strptime(((datetime.utcnow() + timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")),"%Y-%m-%d %H:%M:%S")
    for news in newsList:
        #result = collections.insert(news.get_news_as_json())
        result = collections.update({"source":{"$eq":news.get_source()},"timestamp":{"$gt":fromDate,"$lt":toDate},"headline":news.get_headline()},news.get_news_as_json(),upsert=True)
        #result = collections.update({"source":{"$eq":news.get_source()},"_id":news.get_id()},news.get_news_as_json2(),upsert=True)
        print(result)
    return len(newsList)

def getNews(src,dateStr):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon["collection_newspaper_new"]
    return collections.find_one({'key':dateStr},{'value':1,'_id':0})
    #return collections.find({"source":{"$eq":src},"timestamp":{"$gt":fromDate,"$lt":toDate}})
    #return collections.find({"source":{"$eq":src}})
    
def findNews(src):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    fromDate = datetime.strptime(datetime.utcnow().strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
    toDate = datetime.strptime(((datetime.utcnow() + timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")),"%Y-%m-%d %H:%M:%S")
    return collections.find({"source":{"$eq":src},"timestamp":{"$gt":fromDate,"$lt":toDate}},{'_id':0,'headline': 1})

def getAllNews():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon['collection_newspaper_new']
    return collections.find({})

def removeNews():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon['collection_newspaper_new']
#     fromDate = datetime.strptime(datetime.utcnow().strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
#     toDate = datetime.strptime(((datetime.utcnow() + timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")),"%Y-%m-%d %H:%M:%S")
    return collections.delete_many({})

def getArticleUrls():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    return collections.find({"source":"Navhind"},{'_id':1,'articleUrl':1})

def getNewsDateRange(dateValue):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    fromDate = datetime.strptime(parse(dateValue).strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
    toDate = datetime.strptime(((parse(dateValue) + timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")),"%Y-%m-%d %H:%M:%S")
    return collections.find({"timestamp":{"$gte":fromDate,"$lte":toDate}},{'_id':0})

def getDatesList():
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon[collectionName]
    return collections.distinct("timestamp",{})

def insertNewsDict(newsDict):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon["collection_newspaper_new"]
    count = 0
    for dateStr in newsDict:
        newsjson = {}
        newsjson['key'] = dateStr 
        newsjson['value'] = list(newsDict.get(dateStr))
        count+=1
        #collections.update({},{'$set':dateStr:json_util.dumps(newsDict.get(dateStr))})
        collections.insert(newsjson)
    return count

def appendNewsItem(dateStr,news):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon["collection_newspaper_new"]
#     for newsItem in collections.find({"key":dateStr,"value.$.source":news.get_source(),"value.$.headline":news.get_headline()}):
#         pprint.pprint(newsItem)
    updateResult = collections.update({"key":dateStr,
                                       "value":{"$elemMatch":{"source":news.get_source(),"headline":news.get_headline()}}
                                       },
                            {
                                  '$set':{
                                      'value.$':news.get_news_as_json()
                                    }
                            })
    print(updateResult)
    if not updateResult['updatedExisting']:
        print("inserted")
        return collections.update({"key":dateStr,"source":{"$ne":news.get_source()},"headline":{"$ne":news.get_headline()}},{'$addToSet':{"value":news.get_news_as_json()}},upsert=True)
    else:
        print("updated")
        return updateResult
    
def appendNewsList(newsList):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon["collection_newspaper_new"]
    dateStr = datetime.now().strftime('%Y-%m-%d')
    return collections.update({"key":dateStr},{'$addToSet':{"value":{'$each':list(newsList)}}},upsert=True)

def getNewsHeadlines(src):
    dbCon = dbhandler.mongoConnector.mongoConnection(dbName)
    collections = dbCon['collection_newspaper_new']
    dateStr = datetime.now().strftime('%Y-%m-%d')
    return collections.distinct('value.headline',{"key":dateStr,"value.source":src})