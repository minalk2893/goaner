'''
Created on Jul 18, 2018

@author: minal_t
'''
import requests
from bs4 import BeautifulSoup
from datetime import datetime
from beans.NewspaperDetails import NewspaperDetails
import re
import traceback
#navhindUrl = "http://www.navhindtimes.in/category/goanews"

def scrapNavhind():
    navhindUrl = "http://www.navhindtimes.in/category/goanews"
    iterationStatus = True
    newsList = []
    while(iterationStatus):
        req = requests.get(navhindUrl)
        soup = BeautifulSoup(req.content,'html.parser')
        #print(soup.select("div.post-listing article"))
        #print(soup.select("div.pagination span.current")[0].find_next('a').get('href'))
        for listItem in soup.select("div.post-listing article"):
            newsDate = datetime.strptime(listItem.select("p.post-meta span")[0].text, '%B %d, %Y')
            if (newsDate.date() < datetime.today().date()):
                iterationStatus = False
            else:
                spanArray = listItem.select("h2 a")
                print(spanArray[0].text)
                if len(spanArray) > 0:
                    articleUrl = spanArray[0].get('href')
                    newsDetails = scrapArticle(BeautifulSoup(requests.get(articleUrl).content,'html.parser'),articleUrl)
                    if newsDetails:
                        newsList.append(newsDetails.get_news_as_json())
                
        if(iterationStatus):
            navhindUrl = soup.select("div.pagination span.current")[0].find_next('a').get('href')
    
    return newsList
    
def scrapArticle(article,articleUrl):
    try:
        articleSoup = article.select("#main-content article.post-listing")[0]
        headline = articleSoup.select("div.post-inner h1 span")[0].text
        articleTimestamp = datetime.strptime(articleSoup.select("div.post-inner p span")[1].text, '%B %d, %Y')
        #print(articleSoup.select("div.post-inner div.entry p"))
        place = ''
        firstPara = ''
        paragraphSoup = articleSoup.select("div.post-inner div.entry p")
        if len(paragraphSoup) > 2:
            for paraRef in paragraphSoup:
                if ':' in paraRef.text:
                    place = paraRef.text.split(":",1)[0]
                    firstPara = paraRef.text.split(":",1)[1]
                    print(place)
                    break
                elif paraRef.text.isupper() and not paraRef.find_next("p").text.isupper() and re.compile('[\w@_!#$%^&*()<>?/\|}{~:]+').search(paraRef.find_next("p").text) is not None:
                    place = paraRef.text
                    firstPara = paraRef.find_next("p").text
                    break
            if(place == '' and firstPara == ''):
                place = 'GOA'
                firstPara = ''
        elif len(paragraphSoup) == 1:
            if ':' in paragraphSoup[0].text:
                paraList = paragraphSoup[0].text.split(":",1)
                place = paraList[0]
                firstPara = paraList[1]
        else:
            placeRef = paragraphSoup[2] if len(paragraphSoup[1].text) == 1 else paragraphSoup[1]
            paraRef = placeRef.find_all(text=True)
            place = paraRef[0]
            firstPara = paraRef[1].strip()
        
        imageUrl = articleSoup.select("div.single-post-thumb img")[0].get('src') if len(articleSoup.select("div.single-post-thumb img")) > 0 else ""
    
        return NewspaperDetails(headline, articleTimestamp, place, firstPara, articleUrl, "", imageUrl,"Navhind")
    except:
        traceback.print_exc()
        return False