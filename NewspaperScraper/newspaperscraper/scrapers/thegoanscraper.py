'''
Created on Jul 20, 2018

@author: minal_t
'''
import requests
from bs4 import BeautifulSoup
from datetime import datetime,date
from dateutil.parser import parse
from beans.NewspaperDetails import NewspaperDetails
from select import select

def scrapGoan(pastHeadlines):
    goanUrl = "http://englishnews.thegoan.net/_listing.php?section_id=1"
    iterationStatus = True
    newsList = []
    pageNo = 1
    
    while(iterationStatus):
        print(goanUrl)
        req = requests.get(goanUrl)
        #print(req.content)
        soup = BeautifulSoup(req.content,'html5lib')
        articleId = '#section_%s' %pageNo
        #print(soup.select('#SHOWMORE a'))
        #print(soup)
        for listItem in soup.select("%s div.listing-news-sections" %articleId):
            articleUrl = listItem.select("div.brief-news a")[0].get('href')
            articleSoup = BeautifulSoup(requests.get(articleUrl).content,'html5lib')
            #print(articleSoup.select("div.main-block-1 div.detail-news-section h5 span.date-blue")[0].text)
            newsDate = parse(articleSoup.select("div.main-block-1 div.detail-news-section h5 span.date-blue")[0].text,fuzzy=True)
            #print(newsDate)
            if (newsDate.date() < datetime.today().date()):
                iterationStatus = False
            else:
                if(listItem.select("div.brief-news a h4")[0].text not in pastHeadlines):
                    newsList.append(scrapArticle(articleSoup,articleUrl))
        
        if(iterationStatus):
            pageNo += 1
            if len(soup.select('#SHOWMORE a')) > 0:
                theGoanUrlNext = soup.select('#SHOWMORE a')[0].get('href')
            goanUrl = "%s&page=%s"  %(theGoanUrlNext,pageNo)
            
    return newsList
def scrapArticle(articleSoup,articleUrl):
    articleSection = articleSoup.select("div.main-block-1 div.news-block div.detail-news-section")[0]
    headline = articleSection.select("h1")[0].text
    articleTimestamp = parse(articleSection.select("h5 span.date-blue")[0].text,fuzzy=True)
    newsDetails = articleSection.select("div.display-detailed-news div.detail-content p")
    place = ""
    firstPara = ""
    if(len(newsDetails)>1):
        print(newsDetails)
        for index in range(len(newsDetails)):
            if ':' in newsDetails[index].text:
                splitPara = newsDetails[index].text.split(':',1)
                place = splitPara[0]
                firstPara = splitPara[1] 
                break
            if newsDetails[index].text.strip().isupper() and not newsDetails[index+1].text.strip().isupper():
                firstPara = newsDetails[index+1].text.strip()
                place = newsDetails[index].text.strip()
                break
    else:
        try:
            newsDetailsPara = newsDetails[0].find_all(text=True)
            print(newsDetailsPara)
            #print(newsDetailsPara)
            for index in range(len(newsDetailsPara)):
                if ':' in newsDetailsPara[index]:
                    splitPara = newsDetailsPara.split(':',1)
                    place = splitPara[0]
                    firstPara = splitPara[1]
                    break
                if newsDetailsPara[index].strip().isupper() and not newsDetailsPara[index+1].strip().isupper():
                    firstPara = newsDetailsPara[index+1].strip()
                    place = newsDetailsPara[index].strip()
                    break
        except AttributeError:
            place = newsDetails[0].select("b")[0].text.split(':',1)[0]
            firstPara = newsDetails[0].select("b")[0].find_next_sibling(text=True)
        except:
            print(newsDetails)
    imageUrl = articleSection.select("div.display-detailed-news div.detail-picture img")[0].get('src') if len(articleSection.select("div.display-detailed-news div.detail-picture img"))>0 else ""
    return NewspaperDetails(headline, articleTimestamp, place, firstPara, articleUrl, "", imageUrl,"TheGoan") 