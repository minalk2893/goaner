'''
Created on Jul 19, 2018

@author: minal_t
'''
import requests
from bs4 import BeautifulSoup,Tag
from datetime import datetime
from beans.NewspaperDetails import NewspaperDetails
import re
from cgitb import text

def scrapHeraldo():
    heraldoUrl = "https://www.heraldgoa.in/_listing.php?s_id=1&p_id=0"
    iterationStatus = True
    newsList = []
    pageNo = 1
    
    while(iterationStatus):
        req = requests.get(heraldoUrl)
        soup = BeautifulSoup(req.content,'html.parser')
        articleId = '#section_%s' %pageNo
        
        for listItem in soup.select("%s h3" %articleId):
            newsDate = datetime.strptime(listItem.find_next_sibling("p").text, '%d %b %Y')
            if (newsDate.date() < datetime.today().date()):
                iterationStatus = False
            else:
                linkArray = listItem.select("a")
                #print(linkArray[0].text)
                if len(linkArray) > 0:
                    articleUrl = linkArray[0].get('href')
                    newsDetails = scrapArticle(BeautifulSoup(requests.get(articleUrl).content,'html.parser'),articleUrl)
                    if newsDetails:
                        newsList.append(newsDetails.get_news_as_json())
                
        if(iterationStatus):
            pageNo += 1
            if soup.select(articleId)[0].find_next_sibling("a") is not None:
                heraldoUrlNext = soup.select(articleId)[0].find_next_sibling("a").get('href')
            heraldoUrl = "%s&page=%s"  %(heraldoUrlNext,pageNo)
    return newsList
def scrapArticle(article,articleUrl):
    try:
        articleSoup = article.select("div.main div.container div.row > div")[0]
        headline = articleSoup.select("h1")[0].text
        articleTimestamp = datetime.strptime(articleSoup.select("div.row")[0].select("div h6.date-time span.blue")[0].text.strip(), '%d %b %Y')
        firstParagraph = ""
        if len(articleSoup.find("h3",class_="blue").find_next("div").select("p.share")) == 0:
            for paraDetails in articleSoup.find("h3",class_="blue").find_all_next("div"):
                nextTag = paraDetails.find_next_sibling("div")
                brTag = paraDetails.find("br")
                if (((brTag is None) and re.compile('[\w@_!#$%^&*()<>?/\|}{~:]+').search(paraDetails.text.strip()) is not None) or (brTag is not None and len(paraDetails.text.strip())>1)) and ((nextTag is not None) or (isinstance(nextTag,Tag) and nextTag.name == 'br')):
                    firstParagraph = paraDetails.text
                    break
        else:
            firstParagraph = articleSoup.find("h3",class_="blue").find_next("p").find_next(text=True)
        
        if(':' not in firstParagraph):
            place = "GOA"
            firstPara = firstParagraph
        else:
            splitPara = firstParagraph.split(':',1)
            place = splitPara[0]
            firstPara = splitPara[1]
        imageUrl = articleSoup.select("div.row div div.img-details img")[0].get('src') if len(articleSoup.select("div.row div div.img-details img"))>0 else ""
        return NewspaperDetails(headline, articleTimestamp, place, firstPara, articleUrl, "", imageUrl,"Heraldo")
    except:
        return False