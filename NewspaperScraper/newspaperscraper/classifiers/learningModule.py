'''
Created on Aug 9, 2018

@author: minal_t
'''

from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import json
import re
import nltk
import numpy as np
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.svm import LinearSVC 

def trainModel():
    with open("data.json") as json_data:
        lines = json_data.readlines()
        joined_lines = "["+",".join(lines)+"]"
    
        json_file_data = json.loads(joined_lines)
    data_frame = pd.DataFrame(json_file_data)
    print(data_frame['text'])
    
    stemmer = SnowballStemmer("english")
    words = stopwords.words("english")
    
    data_frame['cleaned'] = data_frame['text'].apply(lambda x: " ".join([stemmer.stem(i) for i in re.sub("[^a-zA-Z]"," ",x).split() if i not in words]).lower())
    
    print(data_frame['cleaned'])
    print(data_frame['category'])
    
    X_train, X_test, y_train, y_test = train_test_split(data_frame['cleaned'], data_frame.category, test_size=0.2)
    
    print(X_train)
    print(y_train)
    
    pipeline = Pipeline([('vect',TfidfVectorizer(ngram_range=(1,2),stop_words="english",sublinear_tf=True)),
                         ('ch1',SelectKBest(chi2,k=10)),
                         ('clf',LinearSVC(C=1.0,penalty='l1',max_iter=3000,dual=False))])
    
    model = pipeline.fit(X_train,y_train)
    
    vectorizer = model.named_steps['vect']
    ch1 = model.named_steps['ch1']
    clf = model.named_steps['clf']
    print(np.argsort(clf.coef_[0]))
    
    feature_names = vectorizer.get_feature_names()
    feature_names = [feature_names[i] for i in ch1.get_support(indices=True)]
    feature_names = np.asarray(feature_names)
    
    target_names = ["Crime","Politics","Events"]
    
    for i,label in enumerate(target_names):
        top10 = np.argsort(clf.coef_[i])[-5:]
        print("%s: %s" %(label," ".join(feature_names[top10])))
    
    print("Accuracy Score: "+ str(model.score(X_test,y_test)))
    print(model.predict(['Killed hotelier to exact revenge for filing bike FIR: Accused: Police recovered a sword and a knife which was allegedly used in the brutal murder of hotelier, Vishwajeet Singh, in the wee hours of Tuesday.']))
    
#     count_vect = CountVectorizer()
#     tfidf_transformer = TfidfTransformer()
#     X_train_counts = count_vect.fit_transform(newsList)
#     print(X_train_counts)
#     print(X_train_counts.shape)
#     X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
#     print(X_train_tfidf)
#     print(X_train_tfidf.shape)