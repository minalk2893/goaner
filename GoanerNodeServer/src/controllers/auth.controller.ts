import {User,UserModel} from '../models/user.model';
import * as jwt from 'jsonwebtoken';
import * as dotenv from 'dotenv';
dotenv.config();

class Auth{
    signIn = (req,res)=>{
        User.findOne({
            username:req.body.username
        },(err,user)=>{
            if(err){
                throw err;
            }

            if(!user){
                res.setStatus(401).send({success:false,msg:"User not found"});
            }else{
                user.comparePassword(req.body.password,(err,isMatch)=>{
                    if(isMatch && !err){
                        let returnToken = this.generateToken(user);
                        res.json(returnToken);
                    }else{
                        res.status(401).send({success:false,msg:"Wrong Password"});
                    }
                });
            }

        }
        );
    }

    signUp = (req,res)=>{
        if(!req.body.username || !req.body.password){
            res.send({success:false,msg:"Please enter username and password"});
        }else{
            let newUser = new User({
                username:req.body.username,
                password:req.body.password
            });

            newUser.save((err)=>{
                if(err){
                    res.send({success:false,msg:"Username already exists"});
                }else{
                	let returnToken = this.generateToken(newUser);
                    res.json(returnToken);
                }
            });
        }
    }
    
    generateToken(user){
    	let token = jwt.sign(user.toJSON(),process.env.JWT_SECRET,{expiresIn:`${process.env.TOKEN_EXPIRATION}m`});
    	return {
    		'accessExpiration':process.env.TOKEN_EXPIRATION,
    		'accessToken':token
    	};
    }
}

export default new Auth();