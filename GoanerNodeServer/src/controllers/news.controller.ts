import {News} from '../models/news.model';

class NewsController{
    getNewsDateRange(req,res){
        News.find({},(err,newsList)=>{
            if(err){
                throw err;
            }else{
                let newsMap:{[k: string]: any} = {};
                newsList.forEach((element) => {
                    newsMap[<string>element.key] = element.value;
                });
                res.json(newsMap);
            }
        });
    }
}

export default new NewsController();