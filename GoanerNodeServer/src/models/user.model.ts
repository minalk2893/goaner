import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt-nodejs';
import * as dotenv from 'dotenv';
dotenv.config();
export interface UserModel extends mongoose.Document{
    username:String,
    password:String,
    comparePassword(password:String,cb:Function);
}

const Schema = mongoose.Schema;
mongoose.set('useCreateIndex',true);


let UserSchema:mongoose.Schema = new Schema({
    username:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
},
{
    timestamps:{
        createdAt:'created_at',
        updatedAt:'updated_at'
    }
});

UserSchema.pre('save',function(next){
    let user:UserModel = <UserModel>this;
    if(this.isModified('password') || this.isNew){
        bcrypt.genSalt(10,(err,salt)=>{
            if(err){
                return next(err);
            }
            bcrypt.hash(user.password,salt,null,(err,hash)=>{
                if(err){
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    }else{
        return next();
    }
});

UserSchema.methods.comparePassword = function(password,cb){
    bcrypt.compare(password,this.password,function(err,isMatch){
        if(err){
            return cb(err);
        }
        cb(null,isMatch);
    });
};

export const User = mongoose.model<UserModel>('User',UserSchema);