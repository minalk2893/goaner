import * as mongoose from 'mongoose';

export interface NewsDetailsModel extends mongoose.Document{
    headline : String,
    timestamp : String,
    place : String,
    firstPara : String,
    articleUrl : String,
    category : String,
    imageUrl : String,
    source : String
}

export interface NewsModel extends mongoose.Document{
    key: String,
    value: [NewsDetailsModel]
}

let NewsDetailsSchema = new mongoose.Schema({
    headline : {
        type:String,
    },
    timestamp : {
        type: String,
    },
    place : {
        type:String
    },
    firstPara : {
        type:String
    },
    articleUrl : {
        type:String
    },
    category : {
        type:String
    },
    imageUrl : {
        type:String
    },
    source : {
        type:String
    }
})

let NewsSchema = new mongoose.Schema({
    key: {
        type: String,
        required:true,
        unique: true
    },
    value: [
        NewsDetailsSchema
    ]
})

NewsSchema.set('collection','collection_newspaper_new');
export const News = mongoose.model<NewsModel>("News",NewsSchema);