import authController from '../controllers/auth.controller';
import * as express from 'express';

const router = express.Router();

router.post('/signin',authController.signIn);
router.post('/signup',authController.signUp);

export default router;