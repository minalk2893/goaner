import NewsController from '../controllers/news.controller';
import * as express from 'express';

const router = express.Router();

router.get('/getNewsDateRange',NewsController.getNewsDateRange);

export default router;