import * as passport from 'passport';
import {Strategy, ExtractJwt} from 'passport-jwt';
import {User} from './models/user.model';

class PassportManager{
    initialize(){
        var opts = {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey : process.env.JWT_SECRET
        }
        passport.use(new Strategy(opts,function(jwt_payload,done){
            User.findOne({id:jwt_payload.sub},function(error,user){
                if(error){
                    return done(error,false);
                }
                if(user){
                    return done(null,user);
                }else{
                    return done(null,false);
                }
            });
        }));
        return passport.initialize();
    }

    authenticate(req,res,next){
        passport.authenticate('jwt',{session:false},(err,user,info)=>{
            if(err){
                return next(err);
            }
            if(!user){
                if(info.name === "TokenExpiredError"){
                    return res.status(401).json({message:'Your token has expired'});
                }else{
                    return res.status(401).json({message:info.message});
                }
            }
            req.user = user;
            return next();
        })(req,res,next);
    }
}

export default new PassportManager();