import server from './app';
import * as dotenv from 'dotenv';
dotenv.config();

server.listen(Number(process.env.SERVER_PORT),process.env.SERVER_HOST,()=>{
    console.log("Server running on port 3000");
});