import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as session from 'express-session';
import AuthRouter from './routes/auth.route';
import NewsRouter from './routes/news.route';
import * as dotenv from 'dotenv';
import passportManager from './passport';
class App{
    public express:express.Application;

    constructor(){
        this.express = express();
        this.config();
        this.loadRoutes();
        dotenv.config();
    }

    public config(){
        const MONGO_URI  ="mongodb://localhost:27017/db_newspaper_scraper";

        this.express.use(cors());
        this.express.use(bodyParser.urlencoded({extended:false}));
        this.express.use(bodyParser.json());
        this.express.use(passportManager.initialize());
        mongoose.connect(MONGO_URI || process.env.MONGO_URI,{ useNewUrlParser: true } );
    }

    public loadRoutes(){
        this.express.use('/goaner/api/auth',AuthRouter);
        this.express.use('/goaner/api/news',passportManager.authenticate,NewsRouter);
    }
}

export default new App().express;