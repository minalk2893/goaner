"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const news_controller_1 = require("../controllers/news.controller");
const express = require("express");
const router = express.Router();
router.get('/getNewsDateRange', news_controller_1.default.getNewsDateRange);
exports.default = router;
