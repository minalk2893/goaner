"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_controller_1 = require("../controllers/auth.controller");
const express = require("express");
const router = express.Router();
router.post('/signin', auth_controller_1.default.signIn);
router.post('/signup', auth_controller_1.default.signUp);
exports.default = router;
