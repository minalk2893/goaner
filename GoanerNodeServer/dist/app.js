"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const auth_route_1 = require("./routes/auth.route");
const news_route_1 = require("./routes/news.route");
const dotenv = require("dotenv");
const passport_1 = require("./passport");
class App {
    constructor() {
        this.express = express();
        this.config();
        this.loadRoutes();
        dotenv.config();
    }
    config() {
        const MONGO_URI = "mongodb://localhost:27017/db_newspaper_scraper";
        this.express.use(cors());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(bodyParser.json());
        this.express.use(passport_1.default.initialize());
        mongoose.connect(MONGO_URI || process.env.MONGO_URI, { useNewUrlParser: true });
    }
    loadRoutes() {
        this.express.use('/goaner/api/auth', auth_route_1.default);
        this.express.use('/goaner/api/news', passport_1.default.authenticate, news_route_1.default);
    }
}
exports.default = new App().express;
