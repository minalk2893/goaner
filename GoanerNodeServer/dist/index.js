"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const dotenv = require("dotenv");
dotenv.config();
app_1.default.listen(Number(process.env.SERVER_PORT), process.env.SERVER_HOST, () => {
    console.log("Server running on port 3000");
});
