"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NewsDetailsBean {
    constructor(headline, timestamp, place, firstPara, articleUrl, category, imageUrl, source) {
        this.headline = headline;
        this.timestamp = timestamp;
        this.place = place;
        this.firstPara = firstPara;
        this.articleUrl = articleUrl;
        this.category = category;
        this.imageUrl = imageUrl;
        this.source = source;
    }
}
exports.NewsDetailsBean = NewsDetailsBean;
