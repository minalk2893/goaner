"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let NewsDetailsSchema = new mongoose.Schema({
    headline: {
        type: String,
    },
    timestamp: {
        type: String,
    },
    place: {
        type: String
    },
    firstPara: {
        type: String
    },
    articleUrl: {
        type: String
    },
    category: {
        type: String
    },
    imageUrl: {
        type: String
    },
    source: {
        type: String
    }
});
let NewsSchema = new mongoose.Schema({
    key: {
        type: String,
        required: true,
        unique: true
    },
    value: [
        NewsDetailsSchema
    ]
});
NewsSchema.set('collection', 'collection_newspaper_new');
exports.News = mongoose.model("News", NewsSchema);
