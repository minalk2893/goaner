"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const news_model_1 = require("../models/news.model");
class NewsController {
    getNewsDateRange(req, res) {
        news_model_1.News.find({}, (err, newsList) => {
            if (err) {
                throw err;
            }
            else {
                let newsMap = {};
                newsList.forEach((element) => {
                    newsMap[element.key] = element.value;
                });
                res.json(newsMap);
            }
        });
    }
}
exports.default = new NewsController();
