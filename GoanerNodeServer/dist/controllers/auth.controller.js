"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_model_1 = require("../models/user.model");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
class Auth {
    constructor() {
        this.signIn = (req, res) => {
            user_model_1.User.findOne({
                username: req.body.username
            }, (err, user) => {
                if (err) {
                    throw err;
                }
                if (!user) {
                    res.setStatus(401).send({ success: false, msg: "User not found" });
                }
                else {
                    user.comparePassword(req.body.password, (err, isMatch) => {
                        if (isMatch && !err) {
                            let returnToken = this.generateToken(user);
                            res.json(returnToken);
                        }
                        else {
                            res.status(401).send({ success: false, msg: "Wrong Password" });
                        }
                    });
                }
            });
        };
        this.signUp = (req, res) => {
            if (!req.body.username || !req.body.password) {
                res.send({ success: false, msg: "Please enter username and password" });
            }
            else {
                let newUser = new user_model_1.User({
                    username: req.body.username,
                    password: req.body.password
                });
                newUser.save((err) => {
                    if (err) {
                        res.send({ success: false, msg: "Username already exists" });
                    }
                    else {
                        let returnToken = this.generateToken(newUser);
                        res.json(returnToken);
                    }
                });
            }
        };
    }
    generateToken(user) {
        let token = jwt.sign(user.toJSON(), process.env.JWT_SECRET, { expiresIn: `${process.env.TOKEN_EXPIRATION}m` });
        return {
            'accessExpiration': process.env.TOKEN_EXPIRATION,
            'accessToken': token
        };
    }
}
exports.default = new Auth();
