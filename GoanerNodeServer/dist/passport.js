"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const passport_jwt_1 = require("passport-jwt");
const user_model_1 = require("./models/user.model");
class PassportManager {
    initialize() {
        var opts = {
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET
        };
        passport.use(new passport_jwt_1.Strategy(opts, function (jwt_payload, done) {
            user_model_1.User.findOne({ id: jwt_payload.sub }, function (error, user) {
                if (error) {
                    return done(error, false);
                }
                if (user) {
                    return done(null, user);
                }
                else {
                    return done(null, false);
                }
            });
        }));
        return passport.initialize();
    }
    authenticate(req, res, next) {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                return next(err);
            }
            if (!user) {
                if (info.name === "TokenExpiredError") {
                    return res.status(401).json({ message: 'Your token has expired' });
                }
                else {
                    return res.status(401).json({ message: info.message });
                }
            }
            req.user = user;
            return next();
        })(req, res, next);
    }
}
exports.default = new PassportManager();
