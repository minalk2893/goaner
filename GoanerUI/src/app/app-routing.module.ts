import {Routes,RouterModule} from '@angular/router';
import { NewsComponent } from './news/news.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth-guard.service';

const appRoutes:Routes = [
    {path:'',component:NewsComponent,canActivate:[AuthGuard]}
];

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes,{useHash:true})
    ],
    exports:[
        RouterModule
    ]
})
export class AppRoutingModule{

}