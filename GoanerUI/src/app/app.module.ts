import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NewsCardComponent } from './news/news-card/news-card.component';
import { NewsComponent } from './news/news.component';
import { FilterPipe } from './shared/filter.pipe';
import { Select2Module } from 'ng2-select2';
import { AuthModule } from './auth/auth.module';
import { AuthRoutingModule } from './auth/auth-routing.module';
import { AppRoutingModule } from './app-routing.module';
import { SharedService } from './shared/shared.service';
import { TokenInterceptor } from './services/token.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth/auth-guard.service';
import { HttpModule } from '@angular/http';
import { ResponseInterceptor } from './services/response.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewsCardComponent,
    NewsComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    Select2Module,
    FormsModule,
    AuthModule,
    AuthRoutingModule,
    AppRoutingModule
  ],
  providers: [SharedService,{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptor,
    multi:true
  },AuthGuard,{
    provide:HTTP_INTERCEPTORS,
    useClass:ResponseInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
