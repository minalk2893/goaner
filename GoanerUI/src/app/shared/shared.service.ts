import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  searchTextModified = new Subject<string>();
  placeFilterModified = new Subject<string>();
  constructor() { }

  onSearchTextChange(newSearchText:string){
    this.searchTextModified.next(newSearchText);
  }

  onplaceFilterChange(newPlaceFilter:string){
    this.placeFilterModified.next(newPlaceFilter);
  }
}
