import { Pipe, PipeTransform } from '@angular/core';
import { NewsDetails } from '../models/newsdetails.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[], searchText: string,column:string): any[] {
    if(!value) return [];
    if(!searchText) return value;

    if(column == 'place'){
      if(searchText == "ALL"){
        return value;
      }else{
        return value.filter(
          (item:NewsDetails) => {
            return item.place.toLowerCase().includes(searchText.toLowerCase());
          }
        )
      }
    }else{
      return value.filter(
        (item:NewsDetails) => {
          return item.headline.toLowerCase().includes(searchText.toLowerCase()) || item.firstPara.toLowerCase().includes(searchText.toLowerCase()) || item.place.toLowerCase().includes(searchText.toLowerCase()) || item.source.toLowerCase().includes(searchText.toLowerCase());
        }
      );
    }
  }

}
