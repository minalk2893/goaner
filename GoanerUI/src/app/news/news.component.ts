import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NewsService } from './news.service';
import { NewsDetails } from '../models/newsdetails.model';
import { SharedService } from '../shared/shared.service';
import { Response } from '@angular/http';
import { FilterPipe } from '../shared/filter.pipe';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  newsSubscription:Subscription;
  newsList:NewsDetails[] = [];
  searchText:string;
  placeFilter:string;
  newsListDateRange = {};
  dateList = [];
  selectedDate:string = "";
  selectedNews = "";
  filteredList = [];

  constructor(private newsService:NewsService,private sharedService:SharedService) { }

  ngOnInit() {
    this.newsSubscription = this.newsService.getNewsList().subscribe(
      (newsList:{[k:string]:NewsDetails[]}) => {
        console.log("subscription called");
        this.newsListDateRange = newsList;
        this.dateList = Object.keys(this.newsListDateRange);
        this.selectedDate = this.dateList[0];
        this.selectedNews = this.newsListDateRange[this.selectedDate][0];
      }
    )
    this.sharedService.searchTextModified.subscribe(
      (searchText:string) => {
        this.searchText = searchText;
        this.filteredList = new FilterPipe().transform(this.newsListDateRange[this.selectedDate],searchText,'');
        this.selectedNews = this.filteredList[0];
      }
    )

    // this.sharedService.placeFilterModified.subscribe(
    //   (placeFilter:string) => {
    //     this.placeFilter = placeFilter;
    //   }
    // )
  }

  onNewsSelect(news){
    this.selectedNews = news;
  }

  onSelectedDateChange(){
    this.filteredList = new FilterPipe().transform(this.newsListDateRange[this.selectedDate],this.searchText,'');
    this.selectedNews = this.filteredList[0];
    //this.selectedNews = this.newsListDateRange[this.selectedDate][0];
  }

}
