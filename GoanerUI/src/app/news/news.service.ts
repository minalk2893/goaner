import { Injectable } from '@angular/core';
import { NewsDetails } from '../models/newsdetails.model';
import { Subject } from 'rxjs';
import 'rxjs/Rx';
import { DataStorageService } from '../services/data-storage.service';
import { Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  newsList:NewsDetails[] = [];
  newsListDateRange = {};

  constructor(private dataStorageService:DataStorageService) { }

  getNewsList(){
     return this.dataStorageService.fetchNewsDateRange();
  }

  setNewsList(newsList:NewsDetails[]){
    this.newsList = newsList;
  }

  getPlacesList(){
    return this.dataStorageService.getPlacesList();
  }

}
