import { Component, OnInit, Input } from '@angular/core';
import { NewsDetails } from '../../models/newsdetails.model';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {

  @Input("newsItem") newsItem:NewsDetails;
  
  constructor() { }

  ngOnInit() {
  }

}
