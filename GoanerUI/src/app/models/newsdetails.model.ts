export class NewsDetails{
    public headline:string;
    public timestamp:Date;
    public place:string;
    public firstPara:string;
    public articleUrl:string;
    public category:string;
    public imageUrl:string;
    public source:string;

    constructor(headline:string,timestamp:Date,place:string,firstPara:string,articleUrl:string,
        category:string,imageUrl:string,source:string
    ){
        this.headline = headline;
        this.timestamp = timestamp;
        this.place = place;
        this.firstPara = firstPara;
        this.articleUrl = articleUrl;
        this.category = category;
        this.imageUrl = imageUrl;
        this.source = source;
    }
}