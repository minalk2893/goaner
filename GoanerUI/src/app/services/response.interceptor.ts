import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class ResponseInterceptor implements HttpInterceptor{
    
    constructor(private router:Router){}
    
    intercept(request:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
        return next.handle(request).do((event:HttpEvent<any>)=>{
            
        },(err:any)=>{
            if(err instanceof HttpErrorResponse && err.status === 401){
                this.router.navigate(['/signin']);
            }
        })
    }
}