import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { NewsDetails } from '../models/newsdetails.model';
import 'rxjs/Rx';
import { NewsService } from '../news/news.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  serverUrl = "http://localhost:3000/goaner/api/news";
  constructor(private http:HttpClient) { }

  fetchNews(){
    return this.http.get(this.serverUrl+'/getAllNews');
  }

  getPlacesList(){
    return this.http.get(this.serverUrl+'/getPlaceList');
  }

  fetchNewsDateRange(){
    return this.http.get(this.serverUrl+'/getNewsDateRange');
  }
}
