import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private authService:AuthService){}

    intercept(request:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
        if(request.url.indexOf("/goaner/api/news")){
            request = request.clone({
                setHeaders:{
                    Authorization: `Bearer ${this.authService.getToken()}`
                }
            });
        }
        return next.handle(request);
    }
}