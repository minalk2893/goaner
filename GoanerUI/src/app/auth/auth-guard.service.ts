import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from "@angular/router";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthGuard implements CanActivate,CanLoad{

    constructor(private authService:AuthService,private router:Router){}

    canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot){
        if(!this.authService.isAuthenticated()){
            this.router.navigate(['/signin']);
        }
        return this.authService.isAuthenticated();
    }

    canLoad(route:Route):Observable<boolean> | Promise<boolean> | boolean{
        if(!this.authService.isAuthenticated()){
            this.router.navigate(['/signin']);
        }
        return this.authService.isAuthenticated();
    }
}