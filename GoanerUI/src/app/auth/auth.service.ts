import { Response } from "@angular/http";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginResponse } from "./loginresponse.interface";
import { Router } from "@angular/router";

@Injectable()
export class AuthService{
    private token:string;
    private tokenExpiration:number;
    private tokenSavedTime:Date;
    private serverUrl:string = "http://localhost:3000/goaner/api/auth";

    constructor(private http:HttpClient,private router:Router){
        this.setToken();
    }

    signinUser(email:string,password:string){
        return this.http.post<LoginResponse>(this.serverUrl+'/signin',{username:email,password:password}).subscribe(
            (response:LoginResponse) => {
                this.token = response.accessToken;
                localStorage.setItem('currentUser', JSON.stringify({ 
                    token: this.token,
                    expiration:response.accessExpiration,
                    savedTime:new Date()
                }));
                this.setToken();
                this.router.navigate(['/']);
            },
            error => {
              console.log(error);
            }
          );
    }

    signupUser(email:string,password:string){
        return this.http.post(this.serverUrl+'/signup',{username:email,password:password}).subscribe(
            (response:LoginResponse) => {
                this.token = response.accessToken;
                localStorage.setItem('currentUser', JSON.stringify({ 
                    token: this.token,
                    expiration:response.accessExpiration,
                    savedTime:new Date()
                }));
                this.setToken();
                this.router.navigate(['/']);
            },
            error => {
              console.log(error);
            }
          );
    }

    getToken():string{
        if(!this.token){
            this.setToken();
        }
        return this.token;
    }

    isAuthenticated():boolean{
        return this.getToken() != null && !this.isTokenExpired(this.tokenSavedTime,Number(this.tokenExpiration));
    }

    isTokenExpired(tokenSaveTime:Date,tokenExpiration:number):boolean{
        let tokenSaveDate = new Date(new Date(tokenSaveTime));
        let tokenDate = new Date(tokenSaveDate.getTime()+tokenExpiration*60*1000);
        return tokenDate.getTime() < new Date().getTime();
    }

    setToken(){
        let tokenObj = JSON.parse(localStorage.getItem('currentUser'));
        if(tokenObj){
            this.token = tokenObj.token;
            this.tokenExpiration = tokenObj.expiration;
            this.tokenSavedTime = tokenObj.savedTime;
        }else{
            this.token = null;
            this.tokenExpiration = null;
            this.tokenSavedTime = null;
        }
    }

    logout(){
        this.token = null;
        this.tokenExpiration = null;
        this.tokenSavedTime = null;
        localStorage.removeItem('currentUser');
        this.router.navigate(['/signin']);
    }
}