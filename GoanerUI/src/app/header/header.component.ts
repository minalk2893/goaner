import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { NewsService } from '../news/news.service';
import { Response } from '@angular/http';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  searchText:string;
  placeList:string[] = [];

  selectOptions = {
    theme: 'bootstrap'
  }
  constructor(private sharedService:SharedService,private newsService:NewsService,private authService:AuthService) { }

  ngOnInit() {
    // this.newsService.getPlacesList().subscribe(
    //   (response:Response) => {
    //     this.placeList = response.json();
    //     this.placeList.unshift("ALL");
    //   }
    // )
  }

  onSearchTextChange(){
    this.sharedService.onSearchTextChange(this.searchText);
  }

  onplaceFilterChange(data){
    this.sharedService.onplaceFilterChange(data.value);
  }

  onLogout(){
    this.authService.logout();
  }

}
